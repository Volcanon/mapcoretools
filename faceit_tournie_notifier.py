
import argparse
import requests
import pdb
import time

from win10toast import ToastNotifier

MAPCORE_ORGANIZER = "948b5a8c-702e-4140-a4b3-1d0cbfeebd45"


def parse_args():
    parser = argparse.ArgumentParser(description='Notifies Admins when there is a call on a mapcore thing')
    parser.add_argument('--api_key', '-a', help='API Key', required=True)
    parser.add_argument('--token', '-t', help='Login Token', required=True)

    return parser.parse_args()


def get_tournaments(api_key):
    try:
        return requests.get("https://open.faceit.com/data/v4/organizers/{}/championships?offset=0&limit=20".format(MAPCORE_ORGANIZER),
                     headers={'Authorization': "Bearer {}".format(api_key)}).json()
    except Exception as e:
        print("Failed to get Tournaments, passing")
        pass


def get_tickets(tournament_id, token):
    try:
        return requests.get("https://api.faceit.com/tickets/v1/ticket?competitionGuid={}&competitionType=championship&status=open".format(tournament_id),
                     headers={'Authorization': "Bearer {}".format(token)}).json()
    except Exception as e:
        print("Failed to query Tickets API, passing")
        pass


def main():
    args = parse_args()
    tournaments = get_tournaments(args.api_key)
    toaster = ToastNotifier()
    while True:
        print("Beginning ticket scan")
        if tournaments and tournaments.get('items'):
            for tournament in tournaments.get('items'):
                tickets = get_tickets(tournament.get('id'), args.token)
                if tickets and tickets.get('tickets'):
                    toaster.show_toast("Open Tickets In Tournament!",
                                       "Tournament: {}".format(tournament.get('name')))

        print("Ticket scan complete, sleeping 30 seconds")
        time.sleep(30)


if __name__ == "__main__":
    main()
