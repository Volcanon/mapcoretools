

import requests
import re
import pdb
import numpy
import argparse
from datetime import datetime

MAPCORE_NA_HUB_ID = 'dd5c0ffd-3472-4383-963f-e357a124832c'
MAPCORE_EU_HUB_ID = '8f865128-f6a3-4299-ace7-e074d6002c34'


def parse_args():
    parser = argparse.ArgumentParser(description='Uses faceit API and some variables such as date range to give match '
                                                 'variance, mean and stddev score delta')
    parser.add_argument('--api_key', '-a', help='API Key', required=True)
    parser.add_argument('--hub', '-b', help='Hub to evaluate', choices=['eu', 'na'], required=True)
    parser.add_argument('--match_limit', '-m', help='Number of matches to evaluate', type=int, default=400)

    return parser.parse_args()


def get_hub_data(hub_id, offset, api_key):
    return requests.get("https://open.faceit.com/data/v4/hubs/{}/matches?offset={}&limit=100".format(hub_id, offset),
                 headers={'Authorization': "Bearer {}".format(api_key)}).json()


def get_match_ids(mapcorena_hub_data):
    match_ids = list()
    for match in mapcorena_hub_data.get('items'):
        match_ids.append(match.get('match_id'))

    return match_ids


def get_match_stats(match_id, api_key):
    return requests.get("https://open.faceit.com/data/v4/matches/{}/stats".format(match_id),
                 headers={'Authorization': "Bearer {}".format(api_key)}).json()


def get_score_delta(match_data):
    score_pattern = re.compile("(\d+) / (\d+)")
    if not match_data.get('rounds'):
        return

    matcher = score_pattern.search(match_data['rounds'][0]['round_stats']['Score'])

    if matcher:
        return abs(int(matcher.group(1)) - int(matcher.group(2)))


def get_hub_id(region):
    if region == 'eu':
        return MAPCORE_EU_HUB_ID
    return MAPCORE_NA_HUB_ID


def convert_epoch_to_datetime(epoch):
    return datetime.fromtimestamp(epoch).strftime("%Y-%m")


def main():
    args = parse_args()
    hub_id = get_hub_id(args.hub)
    result_dict = dict()

    for offset in range(0, args.match_limit, 100):
        print("Scanning match offset {} to {}".format(offset, offset + 100))
        mapcorena_hub_data = get_hub_data(hub_id, offset, args.api_key)

        for match in mapcorena_hub_data.get('items'):
            if match.get('status') == 'FINISHED':
                match_stats = get_match_stats(match.get('match_id'), args.api_key)
                score_delta = get_score_delta(match_stats)
                if score_delta:
                    year_month = convert_epoch_to_datetime(match.get('started_at'))
                    result_dict.setdefault(year_month, []).append(score_delta)

    for key, value in result_dict.items():
        print("Year/Month: {}".format(key))
        print("Match Count: {}".format(len(value)))
        print("Mean: {}".format(numpy.mean(value)))
        print("Variance: {}".format(numpy.var(value)))
        print("Std Dev: {}".format(numpy.std(value)))
        print("\n")


if __name__ == "__main__":
    main()




